# Test MapKit JS with PHP

Test Apple's MapKit JS with PHP. These tests show how to include maps in web pages using PHP and Apple's MapKit JS.

At the 2018 WWDC Apple introduced [MapKit JS](https://developer.apple.com/maps/mapkitjs/ "MapKit JS developper page"), a Javascript tookit to show Apple Maps in a website. To use this kit you should have an Apple Developper account and a [MapKit JS Key](https://developer.apple.com/videos/play/wwdc2018/508 "Getting and Using a MapKit JS Key").

The code here show how to generate a MapKit token and use it to show maps in web pages.

## Files

* mapkit-js.php

  This file is responsible for the creation of the MapKit JS token and is used by the other PHP files.

* `private` folder

  This folder contain informations from your Apple Developper account. File ending with `-sample` should be renamed and filled with your own data.

* `private/auth-key.p8`

  The MapKit JS key. This key is used to sign the header and payload and create the token that will be used to obtain maps from Apple. The file is used by `mapkit-js.php`.

* `private/apple-developer-team-id`

  This file should contain your Apple Developer Team Identifier. This identifier will be incorporated in the payload part of the MapKit JS token. The file is used by `mapkit-js.php`.

* `private/mapkit-js-key-id`

  This file should contain the MapKit JS Key Identifier corresponding to the MapKit JS Key. This identifier will be incorporated in the header part of the MapKit JS token and will be used by Apple to verify that the signature part of the token is valid. The file is used by `mapkit-js.php`.

* `embed.php`

  A simple web page with an embedded map.
  
* `add-annotations.php`

  A web page with a map and annotations on the map.

* `home.php`

  A web page with a map and a custom callout.

## See also

* [Apple, MapKit JS](https://developer.apple.com/maps/mapkitjs/ "MapKit JS developper page")
* [Apple, Getting and Using a MapKit JS Key](https://developer.apple.com/videos/play/wwdc2018/508)
* [Apple, MapKit JS Documentation](https://developer.apple.com/documentation/mapkitjs)
* [Using openssl to sign for Apple's JWT](https://superuser.com/questions/1258478/how-to-get-ecsda-with-p-256-and-sha256-in-openssl)
* [JSON Web Token](https://jwt.io)
* [Simple PHP code for JWT](https://github.com/ccipher/php-jwt)
* [Not so simple PHP code for JWT](https://github.com/lcobucci/jwt)

## License

[CC0-1.0](./LICENSE).