<!DOCTYPE HTML>
<html lang="en">
<head>
	<title>Test MapKit JS with PHP - Embed</title>
	<meta charset="utf-8" />
	<meta name="robots" content="none">
	<meta name="author" content="Joël Brogniart">
  <script src="https://cdn.apple-mapkit.com/mk/5.x.x/mapkit.js"></script>
  <style>
  #map {
      width: 45%;
      height: 600px;
  }
  </style>
  
	
</head>
<body>
	<h1>Test MapKit JS with PHP - Embed</h1>

<!--
	https://developer.apple.com/maps/mapkitjs/
-->
<div id="map"></div>
<script>
  mapkit.init({ authorizationCallback: function(done) {
    <?php
      include_once "mapkit-js.php";
      printf('    done("%s");', get_token());
    ?>

  }});
  
  var Home = new mapkit.CoordinateRegion(
              new mapkit.Coordinate(48.857925, 2.29463),
              new mapkit.CoordinateSpan(0.005, 0.005)
          );
  var map = new mapkit.Map("map");
  map.region = Home;
</script>
</body>
</html>