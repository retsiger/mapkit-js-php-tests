<!DOCTYPE HTML>
<html lang="en">
<head>
	<title>Test MapKit JS with PHP - Add Annotations</title>
	<meta charset="utf-8" />
	<meta name="robots" content="none">
	<meta name="author" content="Joël Brogniart">
  <script src="https://cdn.apple-mapkit.com/mk/5.x.x/mapkit.js"></script>
  <style>
  #map {
      width: 45%;
      height: 600px;
  }
  </style>
  
	
</head>
<body>
	<h1>Test MapKit JS with PHP - Add Annotations</h1>

<!--
	https://developer.apple.com/maps/mapkitjs/
-->
<div id="map"></div>
<script>

var MarkerAnnotation = mapkit.MarkerAnnotation,
    clickAnnotation;
    //48.857925, 2.29463
var tower = new mapkit.Coordinate(48.85839, 2.29454),
    river = new mapkit.Coordinate(48.858, 2.2903);

  mapkit.init({ authorizationCallback: function(done) {
    <?php
      include_once "mapkit-js.php";
      printf('    done("%s");', get_token());
    ?>

  }});
  var map = new mapkit.Map("map");

  // Setting properties on creation:
  var towerAnnotation = new MarkerAnnotation(tower, { color: "#4eabe9", title: "The Tower", glyphText: "♖" });

  // Setting properties after creation:
  var riverAnnotation = new MarkerAnnotation(river);
  riverAnnotation.color = "#969696"; 
  riverAnnotation.title = "The River";
  riverAnnotation.selected = "true";
  riverAnnotation.glyphText = "⚓️";

  // Add and show both annotations on the map
  map.showItems([towerAnnotation, riverAnnotation]);

  // Drop an annotation where a Shift-click is detected:
  map.element.addEventListener("click", function(event) {
      if(!event.shiftKey) {
          return;
      }

      if(clickAnnotation) {
          map.removeAnnotation(clickAnnotation);
      }

      var coordinate = map.convertPointOnPageToCoordinate(new DOMPoint(event.pageX, event.pageY));
      clickAnnotation = new MarkerAnnotation(coordinate, {
          title: "Click!",
          color: "#c969e0"
      });
      map.addAnnotation(clickAnnotation);
  });
</script>
</body>
</html>