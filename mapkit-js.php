<?php
$datadir = __DIR__ . "/private/";

function base64url_encode(string $data): string{
  return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}

function base64url_decode(string $data): string{
  return base64_decode(strtr($data, '-_', '+/') . str_repeat('=', 3 - (3 + strlen( $data )) % 4));
}

function get_jwt_header(string $kid): string {
  //Header information
  $header = [
    "kid" => $kid, // Key Id (kid claim): the MapKit JS Key ID
    "typ" => "JWT", // Type of token (typ claim): should be JWT
    "alg" => "ES256" // Hashing algorithm (alg claim): should be ES256
  ];
  return json_encode($header);
}

function get_jwt_payload(string $iss): string {
  //Header information
  $payload = [
    "iss" => $iss, // Issuer (iss claim): Apple Developer Team ID
    "iat" => sprintf("%d", time()), // Issued at (iat claim): the time that the token is issued in seconds
    "exp" => sprintf("%d", time() + 1800) // Expiration (exp claim): the expiration time of the token in seconds
  ];
  return json_encode($payload);
}

function get_mapkit_jwt(string $team_id_path, string $mapkit_key_path, string $private_key_path): string {
  $kid = file_get_contents($mapkit_key_path);
  $encoded_header = base64url_encode(get_jwt_header($kid));
  $iss = file_get_contents($team_id_path);
  $encoded_payload = base64url_encode(get_jwt_payload($iss));
  $encoded_data = "$encoded_header.$encoded_payload";
  
  $pkeyid = openssl_pkey_get_private("file://".$private_key_path);
  $signed_data = "";
  if( ! openssl_sign($encoded_data, $signed_data, $pkeyid, OPENSSL_ALGO_SHA256)) {
    return false;
  }
  openssl_free_key($pkeyid);
  $encoded_signed_data = base64url_encode($signed_data);
  // Delimit with second period (.)
  $jwt = "$encoded_data.$encoded_signed_data";

  return $jwt;
}

function get_token(): string {
  $datadir=$GLOBALS['datadir'];
  
  $apple_developer_team_id_file = $datadir . "apple-developer-team-id";
  $mapkit_js_key_id_file = $datadir . "mapkit-js-key-id";
  $private_key_file = $datadir . "auth-key.p8";

  return get_mapkit_jwt($apple_developer_team_id_file, $mapkit_js_key_id_file, $private_key_file);
}

?>
