<!DOCTYPE HTML>
<html lang="fr">
<head>
	<title>Test MapKit JS with PHP - Maison</title>
	<meta charset="utf-8" />
	<meta name="robots" content="none">
	<meta name="author" content="Joël Brogniart">
  <script src="https://cdn.apple-mapkit.com/mk/5.x.x/mapkit.js"></script>
  <style>
  #map {
    width: 45%;
    height: 600px;
  }
  
  a:link, a:visited {
    color: #2aaef5;
    outline: none;
    text-decoration: none;
  }

  .landmark {
    width: 250px;
    padding: 7px 0 0 0;
    background: rgba(247, 247, 247, 0.75);
    border-radius: 5px;
    box-shadow: 10px 10px 50px rgba(0, 0, 0, 0.29);
    font-family: Helvetica, Arial, sans-serif;
    -webkit-transform-origin: 0 10px;
    transform-origin: 0 10px;
  }

  .landmark h1 {
    margin-top: 0;
    padding: 5px 15px;
    background: #2aaef5;
    color: rgba(255, 255, 255, 0.9);
    font-size: 16px;
    font-weight: 300;
  }

  .landmark section {
    padding: 0 15px 5px;
    font-size: 14px;
  }

  .landmark section p {
   margin: 5px 0;
  }

  .info_label {
    font-weight: 600;
  }


  .landmark:after {
    content: "";
    position: absolute;
    top: 7px;
    left: -13px;
    width: 0;
    height: 0;
    margin-bottom: -13px;
    border-right: 13px solid #2aaef5;
    border-top: 13px solid rgba(0, 0, 0, 0);
    border-bottom: 13px solid rgba(0, 0, 0, 0);
  }

  @-webkit-keyframes scale-and-fadein {
    0% {
      -webkit-transform: scale(0.2);
      opacity: 0;
    }

    100% {
      -webkit-transform: scale(1);
      opacity: 1;
    }
  }

  @keyframes scale-and-fadein {
    0% {
      transform: scale(0.2);
      opacity: 0;
    }

    100% {
      transform: scale(1);
      opacity: 1;
      }
  }
  
  </style>
  
	
</head>
<body>
	<h1>Test MapKit JS with PHP - Maison</h1>

<!--
	https://developer.apple.com/maps/mapkitjs/
-->
<div id="map"></div>
<script>
  mapkit.init({ authorizationCallback: function(done) {
    <?php
      include_once "mapkit-js.php";
      printf('    done("%s");', get_token());
    ?>

  }});
// Landmarks data
  var homeLandmarks = [
    { coordinate: new mapkit.Coordinate(45.382715, 4.580428), title: "John Doe", address: "Le bout du sentier", postal_code: "42220", town: "Colombier", phone: "+33 6123 45 67 89", homepage: "https://maison.example.org", email: "John.Doe@example.org" }
  ];
  
  // Landmark annotation callout delegate
//  var CALLOUT_OFFSET = new DOMPoint(-148, -78);
  var CALLOUT_OFFSET = new DOMPoint(-150, -185);
  var landmarkAnnotationCallout = {
      calloutElementForAnnotation: function(annotation) {
          return calloutForLandmarkAnnotation(annotation);
      },

      calloutAnchorOffsetForAnnotation: function(annotation, element) {
          return CALLOUT_OFFSET;
      },

      calloutAppearanceAnimationForAnnotation: function(annotation) {
          return "scale-and-fadein .4s 0 1 normal cubic-bezier(0.4, 0, 0, 1.5)";
      }
  };

  // Landmarks annotations
  var annotations = homeLandmarks.map(function(landmark) {
      var annotation = new mapkit.MarkerAnnotation(landmark.coordinate, {
          callout: landmarkAnnotationCallout,
          color: "#c969e0"
      });
      annotation.landmark = landmark;
      return annotation;
  });

  var map = new mapkit.Map("map", {
    mapType: mapkit.Map.MapTypes.Hybrid
  });
  map.showItems(annotations);

  // Landmark annotation custom callout
  function calloutForLandmarkAnnotation(annotation) {
      var div = document.createElement("div");
      div.className = "landmark";

      var title = div.appendChild(document.createElement("h1"));
      title.textContent = annotation.landmark.title;

      var section = div.appendChild(document.createElement("section"));

      var fullAddress = section.appendChild(document.createElement("p"));
      fullAddress.className = "address";
      var part = document.createElement("span");
      part.className = "info_label";
      part.textContent = "Adresse";
      fullAddress.appendChild(part);
      part = document.createTextNode(" :");
      fullAddress.appendChild(part);
      part = document.createElement("br");
      fullAddress.appendChild(part);
      part = document.createTextNode(annotation.landmark.address);
      fullAddress.appendChild(part);
      part = document.createElement("br");
      fullAddress.appendChild(part);
      part = document.createTextNode(annotation.landmark.postal_code + " " + annotation.landmark.town);
      fullAddress.appendChild(part);

      var phone = section.appendChild(document.createElement("p"));
      phone.className = "phone";
      part = document.createElement("span");
      part.className = "info_label";
      part.textContent = "Mobile";
      phone.appendChild(part);
      part = document.createTextNode(" : " + annotation.landmark.phone);
      phone.appendChild(part);

      var homepage = section.appendChild(document.createElement("p"));
      homepage.className = "homepage";
      part = document.createElement("span");
      part.className = "info_label";
      part.textContent = "Page personnelle";
      homepage.appendChild(part);
      part = document.createTextNode(" : ");
      homepage.appendChild(part);
      part = document.createElement("a");
      part.href = annotation.landmark.homepage;
      part.textContent = annotation.landmark.homepage;
      homepage.appendChild(part);

      var email = section.appendChild(document.createElement("p"));
      email.className = "email";
      part = document.createElement("span");
      part.className = "info_label";
      part.textContent = "Courriel";
      email.appendChild(part);
      part = document.createTextNode(" : ");
      email.appendChild(part);
      part = document.createElement("a");
      part.href = "mailto:" + annotation.landmark.email;
      part.textContent = annotation.landmark.email;
      email.appendChild(part);

      return div;
  }

</script>
</body>
</html>
