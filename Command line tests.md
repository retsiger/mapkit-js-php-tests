# Test MapKit JS token with command line

Tests Apple's MapKit JS with the command line.

## Create a folder for our tests

`mkdir tests ; cd tests`

##Extract the public key from an Apple MapKit JS key

`openssl ec -in ../private/auth-key.p8 -pubout -out auth-key-public.p8`

##Get a token and save it to a file

`php -r 'include "../mapkit-js.php"; file_put_contents("token", get_token());'`

The token has the form `header.payload.signature`. Header, payload and signature base64 encoded.

##Extract the header and payload of the token to a file

`awk -F. '{printf "%s.%s", $1, $2 > "header-payload"; }' token`

##Extract the base64 encoded signature to a file

`awk -F. '{printf "%s", $3 > "header-payload.sig.b64"; }' token`

##Decode the signature

`php -r 'include "../mapkit-js.php"; file_put_contents("header-payload.sig", base64url_decode(file_get_contents("header-payload.sig.b64")));'`

##Verify the signature

`openssl dgst -sha256 -verify auth-key-public.p8 -signature header-payload.sig header-payload`

## License

[CC0-1.0](./LICENSE).